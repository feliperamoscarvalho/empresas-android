package br.com.feliperamoscarvalho.empresasandroid.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.feliperamoscarvalho.empresasandroid.R;
import br.com.feliperamoscarvalho.empresasandroid.domain.Enterprise;
import br.com.feliperamoscarvalho.empresasandroid.utils.Config;

import static com.bumptech.glide.load.engine.DiskCacheStrategy.RESULT;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnterpriseFragment extends Fragment {

    private Enterprise enterprise;

    public EnterpriseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enterprise, container, false);
        enterprise = (Enterprise) getArguments().getSerializable("enterprise");
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView txtEnterpriseDesc = view.findViewById(R.id.txtDescription);
        ImageView imgEnterprise = view.findViewById(R.id.imgEnterprise);

        if(enterprise != null){
            txtEnterpriseDesc.setText(enterprise.getDescription());

            String urlPhoto = Config.BASE_URL + enterprise.getPhoto();

            Glide.with(view.getContext())
                    .load(urlPhoto)
                    .error(R.drawable.ic_launcher_background)
                    .diskCacheStrategy(RESULT)
                    .into(imgEnterprise);
        }
    }
}
