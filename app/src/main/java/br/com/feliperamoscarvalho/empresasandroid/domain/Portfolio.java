package br.com.feliperamoscarvalho.empresasandroid.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Portfolio implements Serializable {

    @SerializedName("enterprises_number")
    @Expose
    private Integer enterprisesNumber;

    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> enterprises = null;

    public Integer getEnterprisesNumber() {
        return enterprisesNumber;
    }

    public void setEnterprisesNumber(Integer enterprisesNumber) {
        this.enterprisesNumber = enterprisesNumber;
    }

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }

}