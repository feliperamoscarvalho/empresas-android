package br.com.feliperamoscarvalho.empresasandroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import br.com.feliperamoscarvalho.empresasandroid.R;
import br.com.feliperamoscarvalho.empresasandroid.domain.Enterprise;
import br.com.feliperamoscarvalho.empresasandroid.fragments.EnterpriseFragment;

public class EnterpriseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise);

        Intent intent = getIntent();
        Enterprise enterprise = (Enterprise) intent.getSerializableExtra("enterprise");

        //Configura Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        if(toolbar != null){
            setSupportActionBar(toolbar);
        }
        ImageView imgToolbar = toolbar.findViewById(R.id.logo);
        imgToolbar.setVisibility(View.INVISIBLE);
        getSupportActionBar().setTitle(enterprise.getEnterpriseName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState == null){
            Bundle bundle = new Bundle();
            bundle.putSerializable("enterprise", enterprise);
            EnterpriseFragment fragment = new EnterpriseFragment();
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().add(R.id.enterpriseFragment, fragment).commit();
        }
    }
}
