package br.com.feliperamoscarvalho.empresasandroid;

import android.app.Application;

public class EmpresaAndroidApplication extends Application {

    public static EmpresaAndroidApplication instance = null;

    private String enterpriseSearchText;

    public static EmpresaAndroidApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public String getEnterpriseSearchText() {
        return enterpriseSearchText;
    }

    public void setEnterpriseSearchText(String enterpriseSearchText) {
        this.enterpriseSearchText = enterpriseSearchText;
    }
}
