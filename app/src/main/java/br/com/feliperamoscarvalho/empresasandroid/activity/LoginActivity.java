package br.com.feliperamoscarvalho.empresasandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.feliperamoscarvalho.empresasandroid.EmpresaAndroidApplication;
import br.com.feliperamoscarvalho.empresasandroid.R;
import br.com.feliperamoscarvalho.empresasandroid.domain.Investor;
import br.com.feliperamoscarvalho.empresasandroid.utils.Prefs;
import br.com.feliperamoscarvalho.empresasandroid.domain.User;
import br.com.feliperamoscarvalho.empresasandroid.service.APIService;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends BaseActivity {

    private EditText edtEmail;
    private EditText edtPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtEmail = findViewById(R.id.edtLoginEmail);
        edtPassword = findViewById(R.id.edtLoginPassword);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edtEmail.getText().toString();
                String password = edtPassword.getText().toString();
                if(email.isEmpty() || password.isEmpty()){
                    toast(getString(R.string.msg_login_empty_fields));
                }else{
                    User user = new User(email, password);
                    callLoginUser(user);
                }
            }
        });


    }

    private void callLoginUser(User user){

        //Executa a chamada ao serviço que realiza o login do usuário

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);

        Call<Investor> call = service.loginUser(user);
        call.enqueue(new Callback<Investor>() {
            @Override
            public void onResponse(Call<Investor> call, Response<Investor> response) {
                if(response.code() == 200){

                    Headers headers = response.headers();
                    saveClient(headers);

                    Intent intent = new Intent(getContext(), MainActivity.class);
                    startActivity(intent);
                }else{
                    toast(getString(R.string.msg_login_authentication_error));
                }
            }

            @Override
            public void onFailure(Call<Investor> call, Throwable t) {
                toast(getString(R.string.msg_error_communication));
            }
        });

    }

    private void saveClient(Headers headers){

        //Salva os dados do cliente no SharedPreferences

        String accessToken = headers.get("access-token");
        String client = headers.get("client");
        String uid = headers.get("uid");
        Prefs prefs = new Prefs(getContext());
        prefs.saveClient(accessToken, client, uid);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        EmpresaAndroidApplication app = EmpresaAndroidApplication.getInstance();
        app.setEnterpriseSearchText(null);
    }
}
