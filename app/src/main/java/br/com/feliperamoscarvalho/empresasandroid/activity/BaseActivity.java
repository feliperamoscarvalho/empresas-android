package br.com.feliperamoscarvalho.empresasandroid.activity;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import br.com.feliperamoscarvalho.empresasandroid.R;

public class BaseActivity extends AppCompatActivity {

    protected Context getContext(){
        return this;
    }

    protected Activity getActivity(){
        return this;
    }

    protected void toast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    protected void toast(int msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

}
