package br.com.feliperamoscarvalho.empresasandroid.service;

import br.com.feliperamoscarvalho.empresasandroid.domain.EnterpriseList;
import br.com.feliperamoscarvalho.empresasandroid.domain.Investor;
import br.com.feliperamoscarvalho.empresasandroid.domain.User;
import br.com.feliperamoscarvalho.empresasandroid.utils.Config;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    String BASE_URL = Config.BASE_URL + Config.API_VERSION;

    @POST("users/auth/sign_in")
    Call<Investor> loginUser(@Body User user);

    @GET("enterprises?")
    Call<EnterpriseList> getEnterprises(@Header("access-token") String accessToken, @Header("client") String client,
                                        @Header("uid") String uid, @Query("name") String enterpriseName);

}