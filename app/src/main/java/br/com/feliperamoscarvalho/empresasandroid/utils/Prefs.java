package br.com.feliperamoscarvalho.empresasandroid.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {

    private Context context;
    private SharedPreferences preferences;
    private final String FILE_NAME = "empresas.preferencias";
    private final int MODE = 0;
    private SharedPreferences.Editor editor;
    private final String ACCESS_TOKEN = "accessToken";
    private final String CLIENT = "client";
    private final String UID = "uid";

    public Prefs(Context context){

        context = context;
        preferences = context.getSharedPreferences(FILE_NAME, MODE);
        editor = preferences.edit();

    }

    public void saveClient(String acessToken, String client, String uid){

        editor.putString(ACCESS_TOKEN, acessToken);
        editor.putString(CLIENT, client);
        editor.putString(UID, uid);
        editor.commit();

    }

    public String getAcessToken(){
        return preferences.getString(ACCESS_TOKEN, null);
    }

    public String getClient(){
        return preferences.getString(CLIENT, null);
    }

    public String getUid(){
        return preferences.getString(UID, null);
    }


}

