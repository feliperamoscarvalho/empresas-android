package br.com.feliperamoscarvalho.empresasandroid.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import br.com.feliperamoscarvalho.empresasandroid.EmpresaAndroidApplication;
import br.com.feliperamoscarvalho.empresasandroid.R;
import br.com.feliperamoscarvalho.empresasandroid.adapter.EnterpriseAdapter;
import br.com.feliperamoscarvalho.empresasandroid.domain.Enterprise;
import br.com.feliperamoscarvalho.empresasandroid.domain.EnterpriseList;
import br.com.feliperamoscarvalho.empresasandroid.service.APIService;
import br.com.feliperamoscarvalho.empresasandroid.utils.Prefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseActivity {

    protected EnterpriseAdapter adapter;
    protected RecyclerView recyclerView;
    protected TextView txtStarMessage;
    protected EmpresaAndroidApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Configura Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        if(toolbar != null){
            setSupportActionBar(toolbar);
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Configura RecyclerView
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        txtStarMessage = findViewById(R.id.txtStartMessage);

        app = EmpresaAndroidApplication.getInstance();
        if(app.getEnterpriseSearchText() != null){
            FilterSearch filterSearch = new FilterSearch();
            filterSearch.callGetEnterprise(app.getEnterpriseSearchText());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setQueryHint(getString(R.string.menu_search_message));
        searchView.setOnQueryTextListener(new FilterSearch());

        return true;

    }

    private class FilterSearch implements SearchView.OnQueryTextListener{

        @Override
        public boolean onQueryTextSubmit(String query) {

            app.setEnterpriseSearchText(query);
            callGetEnterprise(query);

            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }

        private void callGetEnterprise(String enterpriseName){

            //Executa a chamada ao serviço que realiza a busca de Empresas

            //Oculta a mensagem inicial
            txtStarMessage.setVisibility(View.INVISIBLE);

            //Busca os dados no SharedPreferences
            Prefs prefs = new Prefs(getContext());
            String accessToken = prefs.getAcessToken();
            String client = prefs.getClient();
            String uid = prefs.getUid();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(APIService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            APIService service = retrofit.create(APIService.class);

            Call<EnterpriseList> call = service.getEnterprises(accessToken, client, uid, enterpriseName);
            call.enqueue(new Callback<EnterpriseList>() {
                @Override
                public void onResponse(Call<EnterpriseList> call, Response<EnterpriseList> response) {
                    EnterpriseList enterprises = response.body();
                    List<Enterprise> enterpriseList = enterprises.getEnterprises();

                    //Adiciona o adapter do RecyclerView
                    adapter = new EnterpriseAdapter(getContext(), enterpriseList);
                    recyclerView.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<EnterpriseList> call, Throwable t) {
                    toast(getString(R.string.msg_error_communication));
                }
            });
        }
    }
}
