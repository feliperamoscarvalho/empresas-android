package br.com.feliperamoscarvalho.empresasandroid.domain;

import java.util.ArrayList;
import java.util.List;

public class EnterpriseList {

    private List<Enterprise> enterprises = new ArrayList<Enterprise>();

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }
}
