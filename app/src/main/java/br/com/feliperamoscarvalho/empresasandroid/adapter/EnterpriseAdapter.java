package br.com.feliperamoscarvalho.empresasandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.feliperamoscarvalho.empresasandroid.R;
import br.com.feliperamoscarvalho.empresasandroid.activity.EnterpriseActivity;
import br.com.feliperamoscarvalho.empresasandroid.domain.Enterprise;
import br.com.feliperamoscarvalho.empresasandroid.utils.Config;

import static com.bumptech.glide.load.engine.DiskCacheStrategy.RESULT;

public class EnterpriseAdapter extends RecyclerView.Adapter<EnterpriseAdapter.EnterpriseViewHolder> {

    private final List<Enterprise> enterprises;
    private final Context context;

    public EnterpriseAdapter(Context context, List<Enterprise> enterprises){
        this.context = context;
        this.enterprises = enterprises;
    }

    @NonNull
    @Override
    public EnterpriseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_enterprise, parent, false);
        EnterpriseViewHolder holder = new EnterpriseViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final EnterpriseViewHolder holder, final int position) {

        final Enterprise enterprise = enterprises.get(position);
        holder.txtEnterpriseName.setText(enterprise.getEnterpriseName());
        holder.txtCity.setText(enterprise.getCity());
        holder.txtCountry.setText(enterprise.getCountry());

        String urlPhoto = Config.BASE_URL + enterprise.getPhoto();

        Glide.with(context)
                .load(urlPhoto)
                .error(R.drawable.ic_launcher_background)
                .diskCacheStrategy(RESULT)
                .into(holder.imgEnterprise);


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, EnterpriseActivity.class);
                intent.putExtra("enterprise", enterprises.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.enterprises != null ? this.enterprises.size() : 0;
    }

    public static class EnterpriseViewHolder extends RecyclerView.ViewHolder{

        public TextView txtEnterpriseName;
        public TextView txtCity;
        public TextView txtCountry;
        public ImageView imgEnterprise;
        public CardView parentLayout;

        public EnterpriseViewHolder(View itemView) {
            super(itemView);

            txtEnterpriseName = itemView.findViewById(R.id.txtEnterpriseName);
            txtCity = itemView.findViewById(R.id.txtCity);
            txtCountry = itemView.findViewById(R.id.txtCountry);
            imgEnterprise = itemView.findViewById(R.id.imgEnterprise);
            parentLayout = itemView.findViewById(R.id.card_view);

        }
    }
}
